[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

# Florey Dark Sky App

Example application using express, mocha (with chai assertions), react built for florey code test.

#

## Usage

TODO: Write usage instructions

## Structure

    .
    ├── .babelrc                   babel config file
    ├── .editorconfig              useful for IDE's (standard js)
    ├── .eslintignore              files for eslint to ignore when running
    ├── .git                       git directory
    ├── .gitignore                 gitignore
    ├── node_modules               node modules
    ├── .npmignore                 npm ignore file
    ├── package.json               package json file
    ├── README.md                  Markdown Readme file
    ├── src                        ES7 source files
    ├── test                       ES7 mocha tests
    ├── coverage                   Code coverage reports
    ├── dist                       Transpiled files

## History

TODO: Write history

## License

All Rights Reserved