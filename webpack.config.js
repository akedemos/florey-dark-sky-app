let webpack = require('webpack')
let path = require('path')

module.exports = {
  devtool: 'eval',
  entry: [
    path.resolve(__dirname, 'src/public/js/index.js')
  ],
  output: {
    path: path.join(__dirname, 'dist/public/js'),
    filename: 'index.js'
  },
  target: 'web',
  module: {
    loaders: [
      {
        test: /\.js?$/,
        include: path.resolve(__dirname, 'src/public/js'),
        exclude: [ /node_modules/ ],
        loader: 'babel-loader'
      }
    ]
  },
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  plugins: [
    new webpack.ProvidePlugin({
      // React available globally
      'React': require.resolve('react')
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    })
  ]
}