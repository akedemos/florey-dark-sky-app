
import Express from 'express'
import middlewares from './middlewares'
import applyMiddlewares from './helpers/applyMiddelwares'
import controllers from './controllers'
import fourOhFour from './helpers/404'
import path from 'path'

function init (port, callback) {
  if (!port) {
    throw new Error('Invalid port number:' + port)
  }

  // base server
  let server = new Express()

  // app that runs on the /v1 route (i.e the web service)
  let app = Express()

  // adds default middlewares
  applyMiddlewares(app, middlewares)
  app.use(controllers)

  // use the json 404 on the web service
  app.all('*', function (req, res) {
    fourOhFour(res)
  })

  // load the web service over the /v1 route
  server.use('/v1', app)

  // serve everything in the public folder otherwise
  server.use(Express.static(path.join(__dirname, 'public')))
  // serve the index html file on other routes (let react-router handle 404's)
  server.all('/*', (req, res) => res.status(404).sendFile(path.join(__dirname, 'public', 'index.html')))

  server = server.listen(port, () => {
    callback(null, server)
  })
}

export default init