import respond from './respond'

export default function fourOhFour (res) {
  let err = new Error('Unknown Route')
  err.code = 404
  respond(err, null, res)
}
