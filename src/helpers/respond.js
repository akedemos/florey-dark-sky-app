import * as _ from 'lodash'

/**
 * This function will send a response. If an error is passed it will respond with status err.code || 500.
 * Otherwise it will respond with status 200 and the data provided in JSON format
 *
 * responses will always look as follows
 *
 * {
 *    status: {
 *      success: bool
 *      error: { // only if !success
 *        message: string
 *       }
 *       version: 'v1'
 *    }
 *    data: { ...data } // or null if !success
 * }
 *
 * @param err
 * @param data
 * @param res
 */
export default function respond (err, data, res) {
  let response = {
    status: {
      success: true,
      version: 'v1'
    },
    data: null
  }

  if (err instanceof Error || err !== null) {
    response.status.success = false
    if (err.message) {
      _.set(response, 'status.error.message', err.message)
    } else if (err.errorMessage) {
      _.set(response, 'status.error.message', err.errorMessage)
    } else {
      response.status.error = err
    }
    if (!_.get(response, 'status.error.message')) {
      _.set(response, 'status.error.message', 'Unknown Error')
    }

    res.status(err.code || 200)
    res.json(response)
    return
  }

  response.data = data
  res.status(200)
  res.json(response)
}
