import _ from 'lodash'

function applyMiddlewares (router, middlewares) {
  _.forEach(middlewares, (m) => {
    router.use(m)
  })
}

export default applyMiddlewares
