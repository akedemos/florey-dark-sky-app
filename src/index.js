import init from './init'

// TODO: Handle env vars correctly (perhaps use dotenv?) and then sanity check
let {PORT} = process.env

init(PORT, () => {
  console.log('server started on port:' + PORT)
})