// TODO: this should be sanity checked at server init!
let { DARKSKY_API_KEY } = process.env

/**
 * This middleware will attach the darksky api key to the request for usage further down the chain.
 * It can be used in controllers where this key is necessary
 * @param req
 * @param res
 * @param next
 */
export default function darkskyMiddleware (req, res, next) {
  req.darkSkyApiKey = DARKSKY_API_KEY
  next()
}