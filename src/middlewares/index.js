import morgan from 'morgan'
import cors from 'cors'
import bodyParser from 'body-parser'

const middlewares = [
  morgan('combined'),
  cors({
    credentials: true,
    origin: /.*/
  }),
  bodyParser.json()
]

export default middlewares
