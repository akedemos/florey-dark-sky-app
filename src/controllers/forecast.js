import express from 'express'
import request from 'request'
import _ from 'lodash'
const router = express.Router()

import respond from '../helpers/respond'
import darksky from '../middlewares/darksky'

// TODO: this request can be improved by passing the qs into the url to allow for some data stripping
router.get('/:location', darksky, function (req, res) {
  let location = _.get(req, 'params.location')
  let url = `https://api.darksky.net/forecast/${req.darkSkyApiKey}/${location}`

  request.get({ url, json: true }, (err, darkskyRes, body) => {
    // Identify bad requests and pass the status and error code down
    if (darkskyRes.statusCode !== 200) {
      err = {
        message: darkskyRes.statusMessage || 'Unknown Error',
        code: darkskyRes.statusCode
      }
      return respond(err, null, res)
    }
    respond(null, body, res)
  })
})

export default router
