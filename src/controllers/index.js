import express from 'express'
const router = express.Router()
// import controllers
import forecast from './forecast'

// assign routes
router.use('/forecast', forecast)

export default router
